import json
import socket
import sys

from math import sqrt, pi

class PID(object):
    def __init__(self, P, I, D, tick = None, integrator_state = 0):
        self.KP = P
        self.KI = I
        self.KD = D
        self.tick = tick
        self.integrator = integrator_state
        self.preverror = 0

    def control(self, target, current):
        err = target - current

        self.integrator += err

        term_P = self.KP * err
        term_D = self.KD * (err - self.preverror)
        term_I = self.KI * self.integrator

        self.preverror = err
        #print("PID  P: {} I:{}".format(term_P, term_I))
        return term_P + term_I + term_D

    def reset(self):
        self.integrator = 0
        self.preverror = 0

class TrackPiece(object):
    def __init__(self, piece):
        if 'length' in piece:
            self.length = piece['length']
            self.is_straight = True
        elif 'radius' in piece and 'angle' in piece:
            self.radius = piece['radius']
            self.angle = piece['angle']
            self.is_straight = False

        else:
            print("Parsing track failed")

        self.switchable = 'switch' in piece and piece['switch'] == True

    # does not take the lane into account
    def get_length(self):
        if self.is_straight:
            return self.length
        else:
            return (2* pi * self.radius) * abs(self.angle) / 360.0

    # does not take the lane into account
    def calc_speed(self):
        if self.is_straight:
            return 10
        else:
            # a nice magic constant
            #a_n = 0.42
            a_n = 0.41
            return sqrt(a_n * self.radius)


class ProBot(object):

    def __init__(self, socket, name, key):
        self.socket = socket
        self.name = name
        self.key = key
        self.color = None
        self.track_pieces = []

        self.pieceIndex = None
        self.pieceDist = None

        self.speed = 0
        self.target = 0

        #self.speedPID = PID(4.4, 0.004, 0.05)
        self.speedPID = PID(5, 0.0015, 0)

    def msg(self, msg_type, data):
        self.send(json.dumps({"msgType": msg_type, "data": data}))

    def send(self, msg):
        self.socket.send(msg + "\n")

    def join(self):
        return self.msg("join", {"name": self.name,
                                 "key": self.key})

    def throttle(self, throttle):
        self.msg("throttle", throttle)

    def ping(self):
        self.msg("ping", {})

    def run(self):
        self.join()
        self.msg_loop()

    def on_join(self, data, tick):
        print("Joined")
        self.ping()

    def on_recv_car_info(self, data, tick): 
        self.color = data['color']
        print("Car color: {0}".format(self.color))

    # Receive track/player info
    def on_game_init(self, data, tick):
        try:
            track = data['race']['track']
            print("Track name: {0}".format(track['name']))

            # mappailua tahan
            for piece in track['pieces']:
                self.track_pieces.append(TrackPiece(piece))
                

        except IndexError, e:
            print("IndexError: {0}".format(e))

        print("Parsed track with {0} track elements".format(len(self.track_pieces)))

        # print track
        #for p in self.track_pieces:
        #    if p.is_straight:
        #        pass#print("Straight: {}".format(p.length))
        #    else:
        #        print("Bend, radius: {}, angle: {}".format(p.radius, p.angle))
        

    def on_game_start(self, data, tick):
        print("Race started")
        self.ping()

    def on_spawn(self, data, tick):
        self.speedPID.reset()

    def on_car_positions(self, data, tick):
        try:
            for car in data:
                if car['id']['color'] == self.color:

                    currentIndex = car['piecePosition']['pieceIndex']
                    currentDist = car['piecePosition']['inPieceDistance']

                    if self.pieceIndex != None:
                        if currentIndex == self.pieceIndex:
                            # stayed on the same piece
                            dx = currentDist - self.pieceDist
                            speed = dx / 1
                        elif self.track_pieces[self.pieceIndex].is_straight:
                            # coming from a straight piece
                            left_in_old_piece = self.track_pieces[self.pieceIndex].length - self.pieceDist
                            dx = left_in_old_piece + currentDist
                            speed = dx / 1
                        else:
                            # coming from a bend, use old speed
                            speed = self.speed                        
                    else:
                        speed = 0

                    tracknow = self.track_pieces[currentIndex]
                    #tracknxt = self.track_pieces[(currentIndex + 1) % len(self.track_pieces)]
                    #tracknxt2 = self.track_pieces[(currentIndex + 2) % len(self.track_pieces)]

                    target = min(10, tracknow.calc_speed())

                    #if tracknow.is_straight:
                    #    print("Straight {:.2f}/{:.2f}".format(currentDist, tracknow.length))
                    #else:
                    #    print("Bend     {:.2f}/{:.2f}".format(currentDist, tracknow.get_length()))

                    lookahead = 1.04 * speed ** 2

                    dist = tracknow.get_length() - currentDist

                    i = 1
                    while dist < lookahead:
                        examined_piece = self.track_pieces[(currentIndex + i) % len(self.track_pieces)]
                        max_speed = examined_piece.calc_speed()

                        if max_speed < target:
                            target = max_speed# * dist / lookahead

                        dist += examined_piece.get_length()
                        i += 1


                    #if tracknow.is_straight and tracknxt.is_straight and tracknxt2.is_straight:
                    #    target = 9.5#8.4
                    #elif tracknow.is_straight and tracknxt.is_straight:
                    #    target = 7.8
                    #else:
                    #    #target = 6.6
                    #    target = min(tracknow.calculate_speed(), tracknxt.calculate_speed())

                    #if target != self.target:
                    #    self.speedPID.reset()

                    throt = self.speedPID.control(target, speed)

                    #print("Target: {:.2f}, Speed: {:.2f}, Throttle: {:.2f}, Lookahead: {:.1f}".format(target, speed, throt, lookahead))

                    self.throttle(max(min(throt, 1.0), 0.0))



                    self.pieceIndex = currentIndex
                    self.pieceDist = currentDist
                    self.speed = speed
                    self.target = target
                    #print("{},{},{}".format(tick, car['piecePosition']['pieceIndex'], car['piecePosition']['inPieceDistance']))
                    #self.throttle(0.5)


        except IndexError, e:
            print("IndexError: {}".format(e))
            self.throttle(0.65)

    def on_crash(self, data, tick):
        print("Someone crashed")
        self.ping()

    def on_game_end(self, data, tick):
        print("Race ended")
        self.ping()

    def on_error(self, data, tick):
        print("Error: {0}".format(data))
        self.ping()

    def msg_loop(self):
        msg_map = {
            'join': self.on_join,
            'yourCar': self.on_recv_car_info,
            'gameInit': self.on_game_init,
            'gameStart': self.on_game_start,
            'carPositions': self.on_car_positions,
            'crash': self.on_crash,
            'spawn': self.on_spawn,
            'gameEnd': self.on_game_end,
            'error': self.on_error,
        }
        socket_file = s.makefile()
        line = socket_file.readline()
        while line:
            msg = json.loads(line)
            msg_type, data, tick = msg['msgType'], msg['data'], msg['gameTick'] if 'gameTick' in msg else None
            if msg_type in msg_map:
                msg_map[msg_type](data, tick)
            else:
                print("Unhandled message: {0}".format(msg_type))
                self.ping()
            line = socket_file.readline()


if __name__ == "__main__":
    if len(sys.argv) != 5:
        print("Usage: ./run host port botname botkey")
    else:
        host, port, name, key = sys.argv[1:5]
        print("Connecting with parameters:")
        print("host={0}, port={1}, bot name={2}, key={3}".format(*sys.argv[1:5]))
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.connect((host, int(port)))
        bot = ProBot(s, name, key)
        bot.run()
